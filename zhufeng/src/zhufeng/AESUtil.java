package zhufeng;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * AES加解密
 * 
 * Created by yyh on 2015/10/9.
 */
public class AESUtil {

	/**
	 * 密钥算法
	 */
	private static final String ALGORITHM = "AES";
	/**
	 * 加解密算法/工作模式/填充方式
	 */
	private static final String ALGORITHM_STR = "AES/ECB/PKCS5Padding";

	/**
	 * SecretKeySpec类是KeySpec接口的实现类,用于构建秘密密钥规范
	 */
	private SecretKeySpec key;

	public AESUtil(String hexKey) {
		key = new SecretKeySpec(hexKey.getBytes(), ALGORITHM);
	}

	/**
	 * AES加密
	 * 
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public String encryptData(String data) throws Exception {
		Cipher cipher = Cipher.getInstance(ALGORITHM_STR); // 创建密码器
		cipher.init(Cipher.ENCRYPT_MODE, key);// 初始化
		return bytesToHexString(cipher.doFinal(data.getBytes()));
	}

	/**
	 * AES解密
	 * 
	 * @param base64Data
	 * @return
	 * @throws Exception
	 */
	public String decryptData(String hexData) throws Exception {
		Cipher cipher = Cipher.getInstance(ALGORITHM_STR);
		cipher.init(Cipher.DECRYPT_MODE, key);
		return new String(cipher.doFinal(hexStringToBytes(hexData)));
	}

	public static String bytesToHexString(byte[] src) {
		StringBuilder stringBuilder = new StringBuilder("");
		if (src == null || src.length <= 0) {
			return null;
		}
		for (int i = 0; i < src.length; i++) {
			int v = src[i] & 0xFF;
			String hv = Integer.toHexString(v);
			if (hv.length() < 2) {
				stringBuilder.append(0);
			}
			stringBuilder.append(hv);
		}
		return stringBuilder.toString();
	}

	/**
	 * Convert hex string to byte[]
	 * 
	 * @param hexString
	 *            the hex string
	 * @return byte[]
	 */
	public static byte[] hexStringToBytes(String hexString) {
		if (hexString == null || hexString.equals("")) {
			return null;
		}
		hexString = hexString.toUpperCase();
		int length = hexString.length() / 2;
		char[] hexChars = hexString.toCharArray();
		byte[] d = new byte[length];
		for (int i = 0; i < length; i++) {
			int pos = i * 2;
			d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
		}
		return d;
	}

	/**
	 * Convert char to byte
	 * 
	 * @param c
	 *            char
	 * @return byte
	 */
	private static byte charToByte(char c) {
		return (byte) "0123456789ABCDEF".indexOf(c);
	}

	public static void main(String[] args) throws Exception {
		AESUtil util = new AESUtil("6d6e9751315543b9"); // 密钥
		String s = util.encryptData("1.0.0.1&ios&16696586");
		System.out.println(s); // 加密
		System.out.println(util.decryptData(s)); // 解密
		System.out.println(bytesToHexString(new byte[] {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,32,33,64,65,127}));
	}
}